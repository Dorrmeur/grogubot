#pour accéder aux variables d’environnement on doit import le module os
import os
#import module discord
import discord
from dotenv import load_dotenv
import asyncio
import random

from discord.ext import commands


load_dotenv(dotenv_path="config")


bot = commands.Bot(command_prefix="$")
musics = {}

@bot.command()
async def play(ctx, url):
    client = ctx.guild.voice_client

    if client and client.channel:
        video = Video(url)
        musics[ctx.guild].append(video)
    else:
        channel = ctx.author.voice.channel
        video = Video(url)
        musics[ctx.guild] = []
        client = await channel.connect()
        # await ctx.send(f"Je lance : {video.url}")
        play_song(client, musics[ctx.guild], video)


@bot.event
async def on_ready():
    print("Le bot est prêt !")

@bot.command(name="del")
async def delete(ctx, number: int):
    messages = await ctx.channel.history(limit=number + 1).flatten()

    for each_message in messages:
        await each_message.delete()

bot.run(os.getenv("TOKEN"))